<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function showUser($id)
    {
        return response()->json(User::find($id));
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'username' => 'required|unique:users',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'roles' => 'required',
            'alamat' => 'required',
            'telfon' => 'required',            
        ]);
        
        $user = new User;
        $user->name = $request->input('name');
        $user->username = $request->input('username');
        $user->email = $request->input('email');
        $user->roles = $request->input('roles');
        $user->alamat = $request->input('alamat');
        $user->telfon = $request->input('telfon');
        $user->password = app('hash')->make($request->input('password'));
        $user->api_token = base64_encode(str_random(40));

        $user->save();
        
        return response()->json($user, 201);
    }

    public function update($id, Request $request)
    {
        $user = User::findOrFail($id);
        
        $this->validate($request, [
          'name' => 'required',
          'username' => 'required|unique:users',
          'email' => 'required|email|unique:users',
          'roles' => 'required',
          'alamat' => 'required',
          'telfon' => 'required', 
        ]);

        if($request->input('password') != null){
            $user->password = app('hash')->make($request->input('password'));

            $this->validate($request, [
              'name' => 'required',
              'username' => 'required|unique:users',
              'email' => 'required|email|unique:users',
              'password' => 'required',
              'roles' => 'required',
              'alamat' => 'required',
              'telfon' => 'required', 
            ]);
        }

        if($request->user()->id != $user->id){
            return response('Unauthorized.', 401);    
        }

        $user->name = $request->input('name');
        $user->username = $request->input('username');
        $user->email = $request->input('email');
        $user->roles = $request->input('roles');
        $user->alamat = $request->input('alamat');
        $user->telfon = $request->input('telfon');
        $user->api_token = base64_encode(str_random(40));
        $user->save();
        
        return response()->json($user, 200);
    }

    public function me(Request $request)
    {
        return response()->json($request->user(), 200);
    }

    public function delete($id)
    {
        User::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }

    public function authenticate(Request $request)
    {
        $this->validate($request, [
 
            'email' => 'required',
            'password' => 'required'
      
        ]);
      
        $user = User::where('email', $request->input('email'))->first();
      
        if(app('hash')->check($request->input('password'), $user->password)){
    
            $apikey = base64_encode(str_random(40));
    
            User::where('email', $request->input('email'))->update(['api_token' => "$apikey"]);;
    
            return response()->json(['status' => 'success','api_token' => $apikey]);
    
        }else{
    
            return response()->json(['status' => 'fail'],401);
    
        }
    }
}