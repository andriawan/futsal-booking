<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
$app->get('/', function () use ($app) {
    return "nothing";
});

$app->group(['prefix' => 'api'], function () use ($app) {

    $app->group(['middleware' => 'auth'], function () use ($app) {

        $app->get('users/{id}', ['uses' => 'UserController@showUser']);
    
        $app->delete('users/{id}', ['uses' => 'UserController@delete']);
    
        $app->put('users/{id}', ['uses' => 'UserController@update']);

        $app->get('user/me', ['uses' => 'UserController@me']);
    });

    $app->post('users', ['uses' => 'UserController@create']);
    $app->post('user/auth', ['uses' => 'UserController@authenticate']);
});