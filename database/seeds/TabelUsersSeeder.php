<?php

use Illuminate\Database\Seeder;

class TabelUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'name' => 'Admin',
                'email' => 'admin@example.com',
                'username' => 'admin',
                'alamat' => 'office',
                'telfon' => '08999',
                'password' => '$2y$10$uOuystJ3HZ1avcBlOfYtJe8X8dCuXYVfh8wJFNP8JqLvhBMuSTSRS',
                'is_delete' => 0,
                'roles' => 'Admin',
                'api_token' =>  base64_encode(str_random(40)),
                'remember_token' => 'V8R7FnJ9Vwy12eGIhrTSwncS5RhFyFxQtcwRIxKsuINT9sXMDcJVb2DD9ivR',
                'created_at' => '2018-03-15 06:32:20',
                'updated_at' => '2018-03-16 06:18:38',
            ),
            1 => 
            array (
                'name' => 'users',
                'email' => 'user@example.com',
                'password' => '$2y$10$zF65Iurzq01nLQ2oxloYBe9JJfLceyrDAO3XVBtwI8Z.lO7Oc0l1S',
                'username' => 'users',
                'alamat' => 'office',
                'telfon' => '08999',
                'roles' => 'User',
                'is_delete' => 0,
                'api_token' =>  base64_encode(str_random(40)),
                'remember_token' => '8OXKBApfMizTw7Ok9rFzOQgwnq3LgGfAHvc8UsoN9sEABBekuIM709UajGbS',
                'created_at' => '2018-03-15 06:32:20',
                'updated_at' => '2018-03-16 03:59:05',
            ),
        ));
    }
}
