<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabelUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
			$table->string('name');
            $table->string('email')->unique();
            $table->string('username')->unique();
            $table->string('alamat')->nullable();
            $table->string('telfon')->nullable();
            $table->string('password', 60);
            $table->string('roles');
            $table->string('api_token', 60)->unique();            
			$table->integer('is_delete')->default(0);
			$table->rememberToken();
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
