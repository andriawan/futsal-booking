<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabelSewa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sewa', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('tanggal_booking');
            $table->dateTime('waktu_booking');
            $table->integer('uang_muka');
            $table->integer('biaya_sewa');
            $table->string('status');
            $table->integer('kd_tarif');
            $table->integer('id_pelanggan');
            $table->integer('kode_lapangan');
			$table->integer('is_delete')->default(0);
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sewa');
    }
}
